import { Steps, Button } from 'antd';

import 'antd/dist/antd.css';

const Step = Steps.Step;

const Component = () => {
  return (
    <div>
      <Button type="primary">Primary</Button>
      <Steps current={1}>
        <Step title="Finished" description="This is a description." />
        <Step title="In Progress" description="This is a description." />
        <Step title="Waiting" description="This is a description." />
      </Steps>
    </div>
  )
}

export default Component